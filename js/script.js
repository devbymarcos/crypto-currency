//Preloader
(function (){
    window.addEventListener('load',()=>{
        let preloader = document.querySelector('.preloader');
        setTimeout(()=>{preloader.classList.add('preOff');},200);
        setTimeout(()=>{preloader.style.display = 'none'},900);
    })
})();

//Scroll down function
(function (){
    const links = document.querySelectorAll('header li a');
    for(let link of links){
        link.addEventListener('click',clickHandler);
    }

    function clickHandler(e){
        //Navigacija unutar stranice
        e.preventDefault();
        const href = this.getAttribute('data-link');
        if(href != null){
            const offsetTop = document.querySelector(href).offsetTop;

            scroll({
                top:offsetTop,
                behavior:'smooth'
            });
        }
       console.log(this.getAttribute('data-link'));
    }
})();

//Responsive menu
(function (){
    let res_button = document.querySelector('.responsive .res');
    let counter = false;
    let header_main = document.querySelector('header#main');
    res_button.addEventListener('click',()=>{
        if(counter == false){
            header_main.classList.add('show');
            header_main.classList.remove('hide');

            res_button.classList.add('x-anim-show');
            setTimeout(()=>{
                res_button.setAttribute('src','./images/misc/x-menu-icon.png')
            },300);
            setTimeout(()=>{
                res_button.classList.remove('x-anim-show');
            },900);

            counter = true;
        }else if(counter == true){
            header_main.classList.add('hide');
            header_main.classList.remove('show');

            res_button.classList.add('x-anim-show');
            setTimeout(()=>{
                res_button.setAttribute('src','./images/misc/hamburger-menu-icon.png');
            },300);
            setTimeout(()=>{
                res_button.classList.remove('x-anim-show');
            },900);

            counter = false;
        }
    })
})();

//Home Particles
/* (function (){   
})(); */

//Countdown timer
(function (){
    let daysHTML = document.querySelector('section.home-top .days p');
    let hoursHTML = document.querySelector('section.home-top .hours p');
    let minHTML = document.querySelector('section.home-top .min p');
    let secHTML = document.querySelector('section.home-top .sec p');

    //let brithday = new Date("Oct 1, 2021 4:15:00").getTime();
    let brithday = new Date().getTime() + 9990000;

    let buffer = setInterval(()=>{

        let now = new Date().getTime();
        //Distance between now and brithday
        let distance = brithday - now;
        
        let days = (Math.floor(distance / (1000 * 60 * 60 * 24))).toString();
        let hours = (Math.floor((distance % (1000 * 60 * 60 *24)) / (1000 * 60 * 60))).toString();
        let minutes = (Math.floor((distance % (1000 * 60 * 60)) / (1000*60))).toString();
        let seconds = (Math.floor((distance % (1000*60)) / 1000)).toString();

        //console.log(days , hours , minutes , seconds);

        function format(number){
            if(number.length < 2){
                return `0${number}`;
            }else{
                return `${number}`
            }
        }

        daysHTML.innerText = format(days);
        hoursHTML.innerText = format(hours);
        minHTML.innerText = format(minutes);
        secHTML.innerText = format(seconds);
        


    },1000);
})();

//First chart
(function (){

    let myChart = document.getElementById('myChart').getContext('2d');
    let howBuyChart = new Chart(myChart,{
    
        type:'doughnut',
        data:{
            labels:['Distributed to Community','Reserved Funding','Founders and Team','Advisors','"Bounty" campaign'],
            datasets:[{
                label:'Population',
                data:[
                    75,
                    13,
                    9,
                    2,
                    1
                ],
                backgroundColor:[
                    '#FF6384',
                    '#36A2EB',
                    '#FFCD56',
                    '#FF9063',
                    '#B97EBB'
                ],
                borderWidth:0,
                hoverBorderWidth:1,
                hoverBorderColor:'#fff',
                hoverOffset: 12
            }]
        },
        options:{
            layout:{
               padding:20 
            },
            animation:{
                /*Ova vrednost kao i opacity moze biti na false i kada skrolovanjem dodjemo do sekcije u kojoj se nalazi canvas onda mu zadamo da pocne animacija*/
                animateRotate: true
            },
            tooltips:{
                enabled:true
            },
            plugins:{
                legend:{
                    display:false
                }
            }
        }
    
    });

})();

//Live charts
(function (){

    let bc_chart = document.getElementById('bc');
    let eth_chart = document.getElementById('eth');
    let xrp_chart = document.getElementById('xrp');

    Chart.defaults.color = '#fff';

    let bc_chart_object = new Chart(bc_chart,{
        type:'line',
        data:{
            labels:[2017,2018,2019,2020,2021],
            datasets:[{
                label:'Bitcoin',
                data:[14156.44,3865.95,7422.65,26272.29,58758.56],
                borderColor:'#FFAA00',
                backgroundColor: '#fff'
            }]
        },
        options:{
            plugins:{
                legend:{
                    display:false
                },
                title:{
                    display:true,
                    text:'Bitcoin',
                    color:'rgb(255,255,255)'
                }
            }
        }
    });

    let eth_chart_object = new Chart(eth_chart,{
        type:'line',
        data:{
            labels:[2017,2018,2019,2020,2021],
            datasets:[{
                label:'Ethereum',
                data:[185.63,463.36,225.68,436.49,2170.08],
                borderColor:'#FFAA00',
                backgroundColor: '#fff'
            }]
        },
        options:{
            plugins:{
                decimation:{
                    enabled:true
                },
                legend:{
                    display:false
                },
                title:{
                    display:true,
                    text:'Ethereum',
                    color:'rgb(255,255,255)'
                }
            }
        }
    });

    let xrp_chart_object = new Chart(xrp_chart,{
        type:'line',
        data:{
            labels:[2017,2018,2019,2020,2021],
            datasets:[{
                label:'XRP',
                data:[0.0064,3.6737,0.3642,0.1933,1.1726],
                borderColor:'#FFAA00',
                backgroundColor: '#fff'
            }]
        },
        options:{
            plugins:{
                decimation:{
                    enabled:true
                },
                legend:{
                    display:false
                },
                title:{
                    display:true,
                    text:'XRP',
                    color:'rgb(255,255,255)'
                }
            }
        }
    });



})();
